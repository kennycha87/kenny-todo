# KennyTodo - A free web app Todo with Markdown editor

## Description
This is a Todo webapp with markdown editor, you can create your todo list with amazing MD format it totally free!

## Implementation 1 - run with node

```bash
## 1. install package
npm i

## 2. init prisma
npx prisma migrate dev --name init

## 3. deploy prisma
npx prisma migrate deploy

## 4. start app
npm run dev
```


## Implementation 2 - run with docker
```docker
docker run --name todo-app -p 3000:3000 -v <your location>:/app/prisma -d kennyleunghk/kenny-todo:<version>
```
You can also download the docker image through docker hub: https://hub.docker.com/r/kennyleunghk/kenny-todo

## Internet Deployment
TBC