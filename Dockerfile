# Stage 0
FROM node:lts-alpine

WORKDIR /app

COPY . .

RUN npm i

RUN npx prisma migrate dev --name init

RUN npx prisma migrate deploy

RUN npm run build

# stage 1
# Production image, copy all the files and run next
FROM node:lts-alpine

WORKDIR /app

ENV NODE_ENV production

# add user and group
RUN addgroup --system --gid 1001 nodejs

RUN adduser --system --uid 1001 nextjs

COPY --from=0 /app/public ./public

# Prisma
COPY --from=0 --chown=nextjs:nodejs /app/prisma/sqlite3.db ./prisma/sqlite3.db
COPY --from=0 --chown=nextjs:nodejs /app/prisma/sqlite3.db-journal ./prisma/sqlite3.db-journal
COPY --from=0 --chown=nextjs:nodejs /app/prisma/schema.prisma ./prisma/schema.prisma

# Automatically leverage output traces to reduce image size
# https://nextjs.org/docs/advanced-features/output-file-tracing
COPY --from=0 --chown=nextjs:nodejs /app/.next/standalone ./
COPY --from=0 --chown=nextjs:nodejs /app/.next/static ./.next/static

USER nextjs

EXPOSE 50001

ENV PORT 50001

CMD ["node", "server.js"]
