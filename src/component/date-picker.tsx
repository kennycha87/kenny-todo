import * as React from 'react';
import { type Dayjs } from 'dayjs';
import { Grid, TextField } from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

const isWeekend = (Dayjs: Dayjs) => {
  const day = Dayjs.day();
  return day === 0 || day === 6;
};

type TodoDatePickerProp = {
  value: string | number;
  setValue: (value: any) => void;
  xs: any
}

const TodoDatePicker = ({ value, setValue, xs }: TodoDatePickerProp) => {
  const dateHandler = (date: any) => {
    if (date) {
      setValue(date.$d);
    } else {
      setValue(new Date());
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Grid item xs={xs} sx={{ padding: '5px' }}>
        <DatePicker
          label="Target Completion Date"
          orientation="landscape"
          inputFormat="DD-MM-YYYY"
          shouldDisableDate={isWeekend}
          mask="__-__-____"
          disablePast
          value={value}
          onChange={dateHandler}
          renderInput={(params) => <TextField {...params} fullWidth size="small" />}
        />
      </Grid>
    </LocalizationProvider>
  );
};

export default TodoDatePicker;
