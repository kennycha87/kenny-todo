"use client"

import useHttp from '@hook/useHttp';
import { useDispatch } from 'react-redux';
import { type AppDispatch } from '@store/index';
import SendIcon from '@mui/icons-material/Send';
import MDEditor, { type ContextStore } from '@uiw/react-md-editor';
import { Box, Grid, Stack, Button, Typography  } from '@mui/material';
import { useState, useEffect, type ChangeEvent, type SyntheticEvent } from 'react';

import TodoDatePicker from './date-picker';
import TodoTextField from './todo-form/todo-textfield';

const NewTodo = () => {

  const [todo, setTodo] = useState({
    title: '',
    description: '',
    completedDate: null,
  });

  const [disableForm, setDisableForm] = useState(false);

  const request = useHttp();

  // update title field
  const titleHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setTodo({ ...todo, title: value });
  };

  // update target completion date
  const timeHandler = (date: any) => {
    console.log(typeof date);
    return !date ? setTodo({ ...todo, completedDate: null }) : setTodo({ ...todo, completedDate: date });
  };

  // form submit
  const formHandler = async (e:SyntheticEvent<HTMLFormElement> ) => {
    e.preventDefault();
    await request('add', todo);
    setTodo({
      title: '',
      description: '',
      completedDate: null
    });
  };

  const mdContentHandler = (value: string, event: ChangeEvent<HTMLTextAreaElement>, contextStore: ContextStore) => {
    setTodo({...todo, description: value});
  }

  useEffect(() => {
    if(todo.completedDate !== null && todo.title.trim().length > 1 && todo.description.trim().length > 1) {
      setDisableForm(false);
    }else {
      setDisableForm(true);
    }
  }, [todo]);

  // @ts-ignore
  // @ts-ignore
  return (
    <Stack
      sx={{ padding: '0.5rem', margin: '0.5rem', backgroundColor: 'white' }}
    >
      <Box margin={1}>
        <Typography
          variant="h4"
          color="#0063c8"
          align="center"
          fontWeight={800}
          marginBottom={2}
        >
          Add New
        </Typography>
      </Box>

      <form onSubmit={formHandler}>
        <Grid container>
          <TodoTextField
            required
            label="Title"
            value={todo.title}
            onChange={titleHandler}
            xs={8}
          />
          <TodoDatePicker
            xs={4}
            value={todo.completedDate!}
            setValue={timeHandler}
          />
          <Grid item xs={12} sx={{ padding: '5px' }}>
            <MDEditor
              value={todo.description}
              // this MD editor on change event has e element, need to declare for ts
              onChange={(v,e, s) => mdContentHandler(v!, e!, s!)}
              // previewOptions={{
              //   rehypePlugins: [[rehypeSanitize]],
              // }}
            />
          </Grid>
          <Grid item xs={12} sx={{ padding: '5px' }}>
            <Button
              fullWidth
              variant="contained"
              startIcon={<SendIcon />}
              type="submit"
              disabled={disableForm}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </form>
    </Stack>
  );
};

export default NewTodo;
