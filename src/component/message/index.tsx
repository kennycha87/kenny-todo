"use client"

import { type RootState } from '@store/index';
import { Alert, Backdrop  } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { memo, type FC, useState, useEffect } from 'react';
import { messageActions } from '@store/slice/message-slice';

const Message: FC = () => {
  const [open, setOpen] = useState(false);
  const message = useSelector((state: RootState) => state.msg.message);
  const dispatch = useDispatch();

  useEffect(() => {
    if (message !== '') {
      setOpen(true);
      setTimeout(() => {
        setOpen(false);
        dispatch(messageActions.clearMessage());
      }, 5000);
    }
  }, [message, dispatch]);

  const handleClose = () => {
    setOpen(false);
    dispatch(messageActions.clearMessage());
  };

  return (
    <Backdrop
      sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={open}
      onClick={handleClose}
    >
      <Alert
        sx={{ minWidth: '200px' }}
        severity="success"
        onClose={handleClose}
      >
        {message}
      </Alert>
    </Backdrop>
  );
};

export default memo(Message);
