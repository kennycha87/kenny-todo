import { type Todo } from '@model/todo';
import { useSelector } from 'react-redux';
import { type RootState } from '@store/index';

import CompletedBox from './completed-box';

const CompletedList = () => {
  const completedList = useSelector((state: RootState) => state.todo.completedList);

  return completedList.map((todo: Todo) => (
    <CompletedBox key={todo.id} todo={todo} />
  ));
};

export default CompletedList;
