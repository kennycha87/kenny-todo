import React from 'react';
import dayjs from 'dayjs';
import useHttp from '@hook/useHttp';
import { type Todo } from '@model/todo';
import MDEditor from '@uiw/react-md-editor';
import UndoIcon from '@mui/icons-material/Undo';
import {
  Box,
  Avatar,
  Tooltip,
  IconButton,
  CardHeader,
  CardContent,
} from '@mui/material';

type CompletedBoxProp = {
  todo: Todo
}

const CompletedBox = ({ todo }: CompletedBoxProp) => {
  const request = useHttp();

  const sendBackHandler = async () => {
    await request('sendBack', todo.id);
  };

  return (
    <Box sx={{ marginBottom: '1rem' }}>
      <CardHeader
        sx={{
          bgcolor: '#EAFFD2',
          borderRadius: '20px 0px 0 0',
        }}
        action={
          <Tooltip title="Undo">
            <IconButton size="large" onClick={sendBackHandler}>
              <UndoIcon />
            </IconButton>
          </Tooltip>
        }
        avatar={
          <Avatar sx={{ bgcolor: '#396C00' }} aria-label="recipe">
            C
          </Avatar>
        }
        title={todo.title}
        titleTypographyProps={{ fontWeight: 'bold' }}
        subheader={`Completed: ${dayjs(todo.updatedTime).format(
          'HH:mm:ss D-MMM, YYYY'
        )}`}
        // subheader={todo.updatedTime}
      />
      <CardContent >
        <MDEditor.Markdown source={todo.description} />
      </CardContent>
    </Box>

  );
};

export default CompletedBox;
