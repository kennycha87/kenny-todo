"use client"

import http from '@utils/http';
import { type RootState } from '@store/index';
import Accordion from '@mui/material/Accordion';
import { memo, useState, useEffect } from 'react';
import { todoActions } from '@store/slice/todo-slice';
import { useDispatch, useSelector } from 'react-redux';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Box, Typography, AccordionDetails, AccordionSummary  } from '@mui/material/';

import CompletedList from './completed-list';

const fetchCompletedTodos = async () => http.get("todo/completedList");

const CompletedTodo = () => {
  const dispatch = useDispatch();

  const [expand, setExpand] = useState(false);

  const completedTodos = useSelector((state: RootState) => state.todo.completedList);

  useEffect(() => {
    fetchCompletedTodos().then(res => dispatch(todoActions.setCompleteTodo(res.data.payload)));
  }, [dispatch]);

  return (
    <Box sx={{ margin: '0.5rem' }}>
      <Accordion expanded={expand} onChange={() => setExpand(!expand)}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography variant="h6" color="green" fontWeight="bold">
            Completed Todos {`(${completedTodos.length})`}
          </Typography>
        </AccordionSummary>
        {expand && (
          <AccordionDetails>
            <CompletedList />
          </AccordionDetails>
        )}
      </Accordion>
    </Box>
  );
};

export default memo(CompletedTodo);
