import React from 'react';
import { Grid, TextField  } from '@mui/material';

const TodoTextField = ({ xs, setData, ...rest }: any) => (
  <Grid item xs={xs} sx={{ padding: '5px' }}>
      <TextField
        fullWidth
        size="small"
        onChange={() => setData}
        {...rest}
      />
    </Grid>

);

export default TodoTextField;
