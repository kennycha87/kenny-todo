import { type ChangeEvent } from 'react';
import { type RootState } from '@store/index';
import CloseIcon from '@mui/icons-material/Close';
import { todoActions } from '@store/slice/todo-slice';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Stack, IconButton, InputLabel, FormControl, OutlinedInput, InputAdornment} from '@mui/material'

const SearchBox = () => {
  const searchContent = useSelector((state: RootState) => state.todo.searchContent);

  const dispatch = useDispatch();

  const handleSearchContent = (e: ChangeEvent<HTMLInputElement>) => {
    dispatch(todoActions.setSearchContent(e.target.value));
  }

  const clearSearchContent = () => {
    dispatch(todoActions.setSearchContent(''));
  }

  return (
    <Stack sx={{margin: '0.5rem' }}>
      <Box sx={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
        <FormControl sx={{ m: 1, width: '50%', backgroundColor: "white" }} size='small' variant="outlined">
          <InputLabel size='small' htmlFor="outlined-adornment-password">Search</InputLabel>
          <OutlinedInput
            size='small'
            id="outlined-adornment-password"
            type="text"
            value={searchContent}
            onChange={handleSearchContent}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  size='small'
                  aria-label="toggle password visibility"
                  edge="end"
                  onClick={clearSearchContent}
                >
                  <CloseIcon fontSize='small' />
                </IconButton>
              </InputAdornment>
            }
            label="Search"
          />
        </FormControl>
      </Box>
    </Stack>
  )
}

export default SearchBox