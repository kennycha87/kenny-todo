import moment from 'moment';
import { Grid } from '@mui/material';

const CardHeaderContent = ({createdDate, targetDate}: any) => {
  const HeaderContentProps = {
    item: true,
    xs: 6,
    md: 6,
    sx: {
      fontWeight: 'bold',
    }
  };

  return (
    <Grid container>
      <Grid {...HeaderContentProps} color='#FF4B00'>{`Target Completion: ${moment(targetDate).format('DD-MMM-yyyy')}`}</Grid>
      <Grid {...HeaderContentProps} textAlign='right'>{`Created: ${moment(createdDate).format('DD-MMM-yyyy')}`}</Grid>
    </Grid>
  );
};

export default CardHeaderContent;