import moment from 'moment';
import { memo } from 'react';
import useHttp from '@hook/useHttp';
import { type Todo } from '@model/todo';
import MDEditor  from '@uiw/react-md-editor';
import CloseIcon from '@mui/icons-material/Close';
import DoneOutlineIcon from '@mui/icons-material/DoneOutline';
import {
  Box,
  Grid,
  Button,
  CardHeader,
  CardContent,
  CardActions,
} from '@mui/material';

import CardHeaderContent from './todo-card-header';

const TodoBox = ({ todo }: {todo: Todo}) => {
  const request = useHttp();

  const deletedHandler = async () => {
    console.log("click done");
    await request('remove', todo.id);
  };

  const doneHandler = async () => {
    console.log("click done");
    await request('setComplete', todo.id);
  };

  return (
    <Box
      key={todo.id}
      sx={{
        // backgroundColor: '#FFC300',
        backgroundColor: moment(todo.completedDate).isBefore(moment.now()) ? '#CF0000' : '#FFC300',
        marginBottom: '5px',
        marginRight: '2px',
      }}
    >
      <Box
        sx={{
          marginLeft: '10px',
          backgroundColor: 'white',
        }}
      >
        <CardHeader
          title={todo.title}
          subheader={<CardHeaderContent targetDate={todo.completedDate} createdDate={todo.createdTime} />}
          subheaderTypographyProps={{fontSize: '0.8rem'}}
        />
        <CardContent>
          <MDEditor.Markdown source={todo.description} style={{ whiteSpace: 'pre-wrap' }} />
        </CardContent>
        <CardActions>
          <Grid item xs={8}>
            <Button
              fullWidth
              variant="contained"
              color="success"
              startIcon={<DoneOutlineIcon />}
              onClick={doneHandler}
            >
              Done
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button
              fullWidth
              variant="contained"
              color="error"
              startIcon={<CloseIcon />}
              id={todo.id}
              onClick={deletedHandler}
            >
              Delete
            </Button>
          </Grid>
        </CardActions>
      </Box>
    </Box>
  );
};

export default memo(TodoBox);
