"use client"

import http from '@utils/http';
import { styled } from '@mui/system';
import { memo , useEffect} from 'react';
import { type Todo } from '@model/todo';
import { type AxiosResponse } from 'axios';
import { type RootState } from '@store/index';
import { todoActions } from '@store/slice/todo-slice';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Stack, Typography } from '@mui/material';

import TodoBox from './todo-box';

const MuiTypography = styled(Typography)({
  textAlign: 'center',
  fontSize: '1.2rem',
  fontWeight: 'bold',
});

const TodoList = () => {

  const dispatch = useDispatch();

  const todoList = useSelector((state: RootState) => state.todo.todoList);

  const searchContent = useSelector((state: RootState) => state.todo.searchContent);

  const searchResult = useSelector((state: RootState) => state.todo.searchResult);

  useEffect(() => {
    http.get("/todo/todoList").then((res: AxiosResponse)  => dispatch(todoActions.getTodoList(res.data.data)));
  }, [dispatch]);

  return (
    <Stack sx={{ margin: '0.5rem', backgroundColor: 'f7f7f7' }}>
      { todoList.length === 0 || todoList.length === 0 || (searchContent.length > 0 && searchResult.length === 0) ? (
        <Grid
          container
          sx={{
            minHeight: '40vh',
          }}
        >
          <Grid
            item
            alignSelf="end"
            xs={12}
            sx={{
              padding: '1rem',
              fontSize: '1.2rem',
              fontWeight: 'bold',
            }}
          >
            <MuiTypography>Opps... Found Empty</MuiTypography>
          </Grid>
          <Grid
            item
            xs={12}
            sx={{
              padding: '1rem',
              fontSize: '1.2rem',
              fontWeight: 'bold',
            }}
          >
            <MuiTypography>⇐ Add your Todo here!</MuiTypography>
          </Grid>
        </Grid>
      ) : ( searchResult.length > 0 ? searchResult.map(
            (todo: Todo) =>
              !todo.deleted && todo.status === 1 && <TodoBox key={todo.id} todo={todo} />
          )
          : todoList?.map(
            (todo: Todo) =>
              !todo.deleted && todo.status === 1 && <TodoBox key={todo.id} todo={todo} />
          )
      )}
    </Stack>
  );
};

export default memo(TodoList);
