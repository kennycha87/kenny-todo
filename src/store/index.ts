import { configureStore } from '@reduxjs/toolkit';
import todoSlice from './slice/todo-slice';
import messageSlice from './slice/message-slice';

const store = configureStore({
  reducer: { todo: todoSlice, msg: messageSlice },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export default store;
