import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import { Todo } from '@model/todo';

const INIT_STATE = {
  todoList: [],
  completedList: [],
  searchContent: '',
  searchResult: []
}

type InitState = {
  todoList: Todo[];
  completedList: Todo[];
  searchContent: string;
  searchResult: Todo[];
}

const todoSlice = createSlice({
  name: 'todo',
  initialState: INIT_STATE,
  reducers: {
    addTodo(state: InitState, action: PayloadAction<Todo>) {
      // state.todoList = [action.payload, ...state.todoList];
      state.todoList = [action.payload, ...state.todoList];
    },
    getTodoList(state: InitState, action: PayloadAction<Todo[]>) {
      state.todoList = action.payload;
      // state.completedList = action.payload.completedList;
    },
    setSearchContent(state: InitState, action: PayloadAction<string>){
      // set the search content, for display logic
      state.searchContent = action.payload;
      if(action.payload?.length > 0){
        state.searchResult = state.todoList.filter(todo =>
          todo.title?.includes(action.payload) || todo.description?.includes(action.payload)
        );
      }else{
        state.searchResult = state.todoList;
      }
    },
    setCompleteTodo(state: InitState, action: PayloadAction<Todo[]>){
      if(action.payload?.length > 0){
        state.completedList = action.payload;
      }
    },
    completeTodo(state: InitState, action: PayloadAction<string>) {
      // find the target
      const target = state.todoList.find((todo) => todo.id === action.payload);

      // remove it from todoList
      state.todoList = state.todoList.filter((todo) => todo.id !== target?.id);

      // add it into completed list
      state.completedList = [target!, ...state.completedList]
    },
    sendBackTodo(state: InitState, action: PayloadAction<string>) {
      // find the target
      const target = state.completedList.find(
        (todo) => todo.id === action.payload
      );


      // remove from list
      const removedFromComplete = state.completedList.filter(
        (todo) => todo.id !== target?.id
      );
      // set state
      // add to completed list
      state.todoList = [target!, ...state.todoList];
      state.completedList = removedFromComplete;
    },
    deleteTodo(state: InitState, action: PayloadAction<string>) {
      // remove from list
      state.todoList = state.todoList.filter(
        (todo) => todo.id !== action.payload
      );
    },
  },
});

export const todoActions = todoSlice.actions;

export default todoSlice.reducer;
