import { createSlice } from '@reduxjs/toolkit';

const INIT_STATE = {
  message: '',
};

const messageSlice = createSlice({
  name: 'todo',
  initialState: INIT_STATE,
  reducers: {
    setMessage(state, action) {
      state.message = action.payload;
    },
    clearMessage(state) {
      state.message = '';
    },
  },
});

export const messageActions = messageSlice.actions;

export default messageSlice.reducer;
