import { NextRequest, NextResponse } from 'next/server';

const GET = async (res: NextRequest, param: any) => {
  console.log(param);
  return NextResponse.json({ payload: "I am payload"}, {status: 200});
}

const POST = async (res: NextRequest) => {
  const body = await res.json();
  console.log(body);
  return NextResponse.json({ payload: "I am payload"}, {status: 200});
}

export {
  GET,
  POST
}