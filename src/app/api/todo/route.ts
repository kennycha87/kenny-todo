import query from '@utils/query';
import logger from '@utils/logger';
import { NextResponse } from 'next/server';
import { type NextApiRequest } from 'next';

const getAll = async (req: NextApiRequest) => {
  logger.info('Call api: %j', req.url);
  const result = await query.getTodoList();
  logger.info('Called API %j completed, %j result fetched', req.url, result?.length);
  return NextResponse.json({ status: 200, payload: result, msg: `Fetch todo list completed, ${result?.length} return` }, { status: 200 })
};

export { getAll as GET };
