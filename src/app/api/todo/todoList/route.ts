import query from '@utils/query';
import logger from '@utils/logger';
import { type NextApiRequest } from 'next';
import { NextResponse } from 'next/server';

const getTodoList = async (req: NextApiRequest) => {
  try {
    const result = await query.getTodoList();
    return NextResponse.json({status: 200, data: result, msg: 'Fetch todo list successful'});
  } catch (error: any) {
    throw new Error("Fail to fetch data");
  } finally {
    logger.info('Called API %j completed', req.url);
  }
};

export { getTodoList as GET };