import query from '@utils/query';
import logger from '@utils/logger';
import { NextResponse, type NextRequest } from 'next/server';

const addTodo = async(req: NextRequest) => {
  logger.info('Start call API: %j', req.url);
  const requestBody = await req.json();
  logger.info('Request body: %j', requestBody);

  const result= await query.add(requestBody).then(res => res);
  logger.info('Todo saved to the DB, returning result: %j', result);
  return NextResponse.json(
    { payload: result },
    {status: 201}
  );
};

export { addTodo as PUT };
