import query from '@utils/query';
import { type NextApiRequest } from 'next';
import { NextResponse } from 'next/server';

const setComplete = async (req: NextApiRequest, param: any) => {
  const result = await query.setCompleted(param.params.id);
  return NextResponse.json({ status: 202, payload: result, msg: `id: ${param.params.id} has been set to completed`}, {status: 202})
};

export { setComplete as PATCH };
