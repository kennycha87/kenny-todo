import query from '@utils/query';
import { type NextApiRequest } from 'next';
import { NextResponse } from 'next/server';

const sendBack = async (req: NextApiRequest, param: any) => {
  const result = await query.sendBack(param.params.id);
  return NextResponse.json({ status: 202, payload: result, msg: `id: ${param.params.id} has been send back to Todo List`}, {status: 202})
};

export { sendBack as PATCH };
