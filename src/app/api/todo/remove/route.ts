import query from '@utils/query';
import logger from '@utils/logger';
import { type NextApiRequest } from 'next';
import { NextResponse } from 'next/server';

const remove = async (req: NextApiRequest) => {
  try {
    const result = await query.remove(req.body.id);
    if (result) {
      return NextResponse.json({ msg: `${result.id} is marked to deleted`, data: result });
    }
    return { msg: `${result!.id} is marked to deleted`, data: result };
  } catch (error: any) {
    logger.error('Remove todo fail, error message', error.message);
    return NextResponse.json({ msg: error.message, status: 500  })
  } finally {
    logger.info('Call API: %j completed', req.url);
  }
};

export { remove as DELETE };
