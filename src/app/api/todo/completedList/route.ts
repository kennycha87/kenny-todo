import query from '@utils/query';
import logger from '@utils/logger';
import { type NextApiRequest } from 'next';
import { NextResponse } from 'next/server';

const completedList = async (req: NextApiRequest) => {
  logger.info('Called api: %j', req.url);
  const result = await query.getCompletedList();
  return NextResponse.json({ msg: 'Completed todo list', payload: result });
};

export { completedList as GET};
