"use client"

import React from 'react';
import Head from 'next/head';
import { Grid } from '@mui/material';
import Message from '@component/message';
import SearchBox from '@component/search';
import { useSelector } from 'react-redux';
import NewTodo from '@component/new-todo';
import { type RootState } from '@store/index';
import TodoList from '@component/pending-todo';
import CompletedTodo from '@component/completed-todo';

export default function Home(){
  const message = useSelector((state: RootState) => state.msg.message);

  return (
    <>
      <Head>
        <title>Kenny Todo</title>
      </Head>

      <Grid container sx={{ justifyContent: 'center', padding: '1rem' }}>
        <Grid item xs={12} sx={{ justifyContent: 'center', padding: '1rem' }}>
          <SearchBox />
        </Grid>
        <Grid item xs={5}>
          <Grid container>
            <Grid item xs={12}>
              <NewTodo />
            </Grid>
            <Grid item xs={12}>
              <CompletedTodo />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={5}>
          <TodoList />
        </Grid>
        {message !== '' && <Message />}
      </Grid>
    </>
  )
} ;