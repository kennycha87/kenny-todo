"use client"

import store from '@store/index';
import { Provider } from 'react-redux';
import { type ReactNode }  from 'react';

export default function RootLayout({
  children,
}: {
  children: ReactNode
}) {
  return (
    <html lang="en">
      <body style={{ backgroundColor: "#f8f8f8"}}>
        <main>
          <Provider  store={store}>
            {children}
          </Provider>
        </main>
      </body>
    </html>
  );
}
