import httpClient from '@utils/http';
import { useDispatch } from 'react-redux';
import { type AppDispatch } from '@store/index';
import { todoActions } from '@store/slice/todo-slice';
import { messageActions } from '@store/slice/message-slice';

const useHttp = () => {
  const dispatch = useDispatch<AppDispatch>();

  return async (request: string | { url: string, method: string }, payload: any) => {
    switch (request) {
      case 'add': {
        const result = await httpClient.put('todo/add', payload);
        console.log(result.statusText);
        if(result.statusText === 'Created') {
          const savedTodo = result.data.payload;
          dispatch(todoActions.addTodo(savedTodo));
          dispatch(messageActions.setMessage(`Todo ${savedTodo.id} created`));
          break;
        }
        dispatch(messageActions.setMessage(result.data.msg));
        break;
      }
      case 'setComplete': {
        const result = await httpClient.patch(`todo/setComplete/${payload}`);
        console.log(result.statusText)
        if(result.statusText === 'Accepted'){
          const { data } = result;
          dispatch(todoActions.completeTodo(data.payload.id));
          dispatch(messageActions.setMessage(data.msg));
        }
        break;
      }
      case 'remove': {
        httpClient.delete('todo/remove', payload)
          .then((res) => {
            if (res.statusText === 'OK') {
              const { data, msg } = res.data;
              dispatch(todoActions.deleteTodo(data.id));
              dispatch(messageActions.setMessage(msg));
              return data;
            }
          })
          .catch((err) => {
            // console.log(err.message);
            dispatch(messageActions.setMessage(err.message));
          });
        break;
      }
      case 'sendBack': {
        const result = await httpClient.patch(`todo/sendBack/${payload}`);
        if(result.statusText === 'Accepted') {
          const { data } = result;
          console.log(data);
          dispatch(todoActions.sendBackTodo(data.payload.id));
          dispatch(messageActions.setMessage(data.msg));
        }
        break;
      }
      default: {
        dispatch(messageActions.setMessage('HTTP called error'));
        break;
      }
    }
  };
};

export default useHttp;
