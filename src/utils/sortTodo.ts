import { type Todo } from '@model/todo';
import logger from '@utils/logger';

const compare = (a: Todo, b: Todo) => a.completedDate! - b.completedDate!;

const sortTodos = (todos: Todo[]) => {
  const result = todos.sort(compare);
  logger.info(result);
  return result;
};

export default sortTodos;
