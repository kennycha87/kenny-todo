import { createLogger, transports, format, type Logger } from 'winston';

const logger: Logger = createLogger({
  level: 'info',
  format: format.combine(
    format.timestamp({ format: 'YYYY-MMM-DD HH:mm:ss' }),
    format.splat(),
    format.printf(
      (info) =>
        `[${info.timestamp}] - [${info.level.toUpperCase()}]: ${info.message}`
    )
  ),
  transports: [
    new transports.Console(),
    // new transports.File({ filename: 'app.log' }),
  ],
});

export default logger;
