import axios, { type AxiosInstance } from 'axios';

const httpClient: AxiosInstance = axios.create({
  baseURL: getPath(),
  headers: {
    'Content-Type': 'application/json',
    timeout: 3600,
  },
});

export default httpClient;

function getPath () {
  const hostName = process.env.HOST_NAME;
  if (!hostName || hostName === 'localhost') {
    console.debug("no ENV: 'hostname' provided, use http://localhost instead");
    return "http://localhost:50001/api/"
  }
  return `http://${hostName}:50001/api/`;
}
