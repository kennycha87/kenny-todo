import { PrismaClient } from '@prisma/client';

import logger from './logger';

const prisma = new PrismaClient({
  log: [
    {
      emit: 'event',
      level: 'query',
    },
    {
      emit: 'stdout',
      level: 'error',
    },
    {
      emit: 'stdout',
      level: 'info',
    },
    {
      emit: 'stdout',
      level: 'warn',
    },
  ],
});

const getTodoList = async () => {
  logger.info('Called getTodoList');
  return prisma.todo.findMany({
    where: {
      AND: [{ status: 1 }, { deleted: false }],
    },
    orderBy: { completedDate: 'desc', },
  })
    .then(res =>  res)
    .catch(err => logger.error(err.message))
    .finally(() => disconnect());
};

const getCompletedList = async () => {
  logger.info('Called getCompletedList');
  return prisma.todo.findMany({
    where: {
      status: 0,
    },
    orderBy: {
      updatedTime: 'desc',
    },
  })
    .then(res => {
      logger.info('Fetch completed list success, total records: %j ', res.length);
      return res;
    })
    .catch(err => logger.error(err.message))
    .finally(() => disconnect());
};

const add = async (data: any) => {
  logger.info('Start add todo to database');
  return prisma.todo.create({ data })
    .then(res => {
      logger.info('Create new todo to database, id: %j', res.id);
      return res;
    })
    .catch(err => logger.error(err.message))
    .finally(() => disconnect());
};

const setCompleted = async (id: string ) => {
  logger.info('Start update %j to completed', id);
  return prisma.todo.update({
    where: { id, },
    data: { status: 0, }
  })
    .then(res => {
      logger.info('Id: %j Update completed', id);
      return res;
    })
    .catch(err => logger.error(err.message))
    .finally(() => disconnect());
};

const sendBack = async (id: string) => {
  logger.info('Start send back todo: %j', id);
  return prisma.todo.update({
    where: { id, },
    data: { status: 1 },
  })
    .then(res => {
      logger.info('Id: %j send back to todo', id);
      return res;
    })
    .catch(err => logger.error(err.message))
    .finally(() => disconnect());
};

const remove = async (id: string) => {
  logger.info('Start update %j to deleted', id);
  return prisma.todo.update({
    where: { id, },
    data: { deleted: true, }
  })
    .then(res => {
      logger.info('Id: %j marked to deleted', id);
      return res;
    })
    .catch(err => logger.error(err.message))
    .finally(() => disconnect());
};

const disconnect = async () => {
  logger.info('Close Prisma');
  await prisma.$disconnect();
};

export default {
  add,
  getTodoList,
  getCompletedList,
  setCompleted,
  sendBack,
  remove,
};
