export const todo = {
  id: '',
  title: '',
  description: '',
  status: '',
  deleted: '',
  completedDate: undefined,
  createdTime: undefined,
  updatedTime: undefined
}

export type Todo = {
  id: string | undefined;
  title: string | undefined;
  description: string | undefined;
  status: number | undefined;
  deleted: boolean | undefined;
  completedDate: number | undefined;
  createdTime: number | undefined;
  updatedTime: number | undefined;
}